from django.db import models


class Invoice(models.Model):
    name = models.CharField(max_length=100, null=True,
                            blank=True, verbose_name="Invoice name")
