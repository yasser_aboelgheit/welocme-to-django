from django.shortcuts import render, redirect
from .forms import InvoiceForm
from .models import Invoice


def index2(request):
    if request.method == 'POST':
        form = InvoiceForm(request.POST)
        if form.is_valid():
            instance = form.save(commit=False)
            instance.save()
            return redirect('home2')
    else:
        form = InvoiceForm()

    return render(request, 'home2.html', {'form': form,
                                          'invoices': Invoice.objects.all()})
