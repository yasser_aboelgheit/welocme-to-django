
from django.shortcuts import render, redirect
from django.http import HttpResponse


def index(request):
    context = {}
    context["normal_var"] = "hello I am variable"
    context["bool_var"] = True
    context["country"] = "Egypt"
    context["list"] = [1, 2, 3, 4]
    context["dict"] = {"key1": "value1",
                       "key2": "value2",
                       "key3": "value3"}

    return render(request, 'home.html', context)
